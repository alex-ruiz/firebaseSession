const app = require('firebase/app');
require('firebase/firebase-auth');

const env = process.env;
const firebaseConfig = {
  apiKey: env.REACT_APP_APIKEY,
  authDomain: env.REACT_APP_AUTHDOMAIN,
  databaseURL: env.REACT_APP_DATABASEURL,
  projectId: env.REACT_APP_PROJECTID,
  storageBucket: env.REACT_APP_STORAGEBUCKET,
  messagingSenderId: env.REACT_APP_MESSAGINGSENDERID,
  appId: env.REACT_APP_APPID,
  measurementId: env.REACT_APP_MEASUREMENTID
};

class FirebaseUtil {
  constructor() {
    app.initializeApp(firebaseConfig);

    this.auth = app.auth();
  }

  createFieldPath(fields) {
    return new app.firestore.FieldPath(fields);
  }
}

module.exports = new FirebaseUtil();
