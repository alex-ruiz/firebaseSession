const admin = require('firebase-admin');

const serviceAccount = require(`./${process.env.NAME_FIREBASE_JSON}`);

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: process.env.REACT_APP_DATABASEURL,
});

const {VERIFY_TOKEN} = process.env;
console.log({VERIFY_TOKEN})
admin
  .auth()
  .verifyIdToken(VERIFY_TOKEN)
  .then((userInfo)=>console.log(`verified ${!!userInfo}`))//({userInfo}))
  .catch((e) => console.log('no user', e));
