require('dotenv').config();
const auth = require('./FirebaseUtil').auth;

const {AUTH_EMAIL, AUTH_PASSWORD} = process.env;
auth.signInWithEmailAndPassword(AUTH_EMAIL, AUTH_PASSWORD)
      .then(async ({user}) => {
          console.log({token: await user.getIdToken()})
      })
      .catch((error) => console.log({error}));
