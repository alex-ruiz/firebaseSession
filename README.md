# GET AUTH TOKEN FIREBASE

#### Required
 
 * firebase account
 * file **custom.env** in root, or **.env** if its only one project
    ```javascript
        NAME_FIREBASE_JSON=test-firebase.json

        AUTH_EMAIL=myEmail
        AUTH_PASSWORD=myPassword

        VERIFY_TOKEN=only_if_you_want_to_verify_some_auth_token

        REACT_APP_APIKEY=
        REACT_APP_AUTHDOMAIN=
        REACT_APP_DATABASEURL=
        REACT_APP_PROJECTID=
        REACT_APP_STORAGEBUCKET=
        REACT_APP_MESSAGINGSENDERID=
        REACT_APP_APPID=
        REACT_APP_MEASUREMENTID=
    ```
 * file configuration **firebase.json** in root (opcional), only if you want to verify some auth token

# RUN PROJECT
 * Install dependencies `npm i`
 * Run `npm run start` or `AUTH_EMAIL=myEmail AUTH_PASSWORD=myPassword npm run start` if you using only the .env file for one project
 * Run `FILE_NAME=custom.env npm run start` or `FILE_NAME=custom.env AUTH_EMAIL=myEmail AUTH_PASSWORD=myPassword npm run start` to customize the envs files for use to diferents projects
 * Run if you want to verify token `npm run verifyToken` or `VERIFY_TOKEN=token npm run verifyToken`